//
//  CameraViewController.swift
//  After
//
//  Created by Liam Dunne on 18/01/2016.
//  Copyright © 2016 Homestay. All rights reserved.
//

import UIKit
import AVFoundation
import CoreImage
import CoreLocation
import Photos
import AssetsLibrary
import ImageIO
import MobileCoreServices

enum ActionState {
    case SelectPhoto
    case TakePhoto
    case FinishUp
}

public class ThumbnailCell :UICollectionViewCell {
    @IBOutlet weak var thumbnailImageView: UIImageView!
    @IBOutlet weak var indicatorImageView: UIImageView!

    let selectedColor = UIColor(hue: 0.55, saturation: 0.7, brightness: 1.0, alpha: 1.0)
    
    public override func prepareForReuse() {
        selected = false
        shouldSetSelected(false)
    }
    
    func shouldSetSelected(selected :Bool){
        if selected {
            self.thumbnailImageView.layer.borderColor = selectedColor.CGColor
            self.indicatorImageView.layer.borderWidth = 2.0
            self.indicatorImageView.layer.borderColor = selectedColor.CGColor
            self.indicatorImageView.layer.borderWidth = 2.0
            self.indicatorImageView.layer.cornerRadius = CGRectGetWidth(self.indicatorImageView.frame)/2.0
            self.indicatorImageView.layer.masksToBounds = true
            self.indicatorImageView.layer.backgroundColor = selectedColor.colorWithAlphaComponent(0.8).CGColor
        } else {
            self.thumbnailImageView.layer.borderColor = UIColor.clearColor().CGColor
            self.indicatorImageView.layer.borderWidth = 0.0
            self.indicatorImageView.layer.borderColor = selectedColor.colorWithAlphaComponent(0.333).CGColor
            self.indicatorImageView.layer.borderWidth = 2.0
            self.indicatorImageView.layer.cornerRadius = CGRectGetWidth(self.indicatorImageView.frame)/2.0
            self.indicatorImageView.layer.masksToBounds = true
            self.indicatorImageView.layer.backgroundColor = UIColor.clearColor().CGColor
        }
    }
}

class CameraViewController: UIViewController, UIImagePickerControllerDelegate, UINavigationControllerDelegate, UIGestureRecognizerDelegate, AVCaptureMetadataOutputObjectsDelegate, PHPhotoLibraryChangeObserver,
UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout {
    @IBOutlet weak var cameraView: UIView!
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var actionButton: UIButton!
    @IBOutlet weak var addImageBarButtonItem: UIBarButtonItem!
    @IBOutlet weak var imageWidthConstraint: NSLayoutConstraint!
    @IBOutlet weak var imageHeightConstraint: NSLayoutConstraint!
    @IBOutlet var pan: UIPanGestureRecognizer!
    
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var collectionViewFlowLayout: UICollectionViewFlowLayout!
    
    @IBOutlet weak var collectionViewBottomConstraint: NSLayoutConstraint!
    @IBOutlet weak var collectionViewHeightConstraint: NSLayoutConstraint!

    let session         : AVCaptureSession = AVCaptureSession()
    var previewLayer    : AVCaptureVideoPreviewLayer!
    let stillImageOutput = AVCaptureStillImageOutput()
    var orientation: UIImageOrientation = .Up
    var initialPanPoint :CGPoint? = CGPointZero

    var beforeImage :UIImage? = nil
    var beforeImageEXIFData :NSDictionary? = nil
    var filteredBeforeImage :UIImage? = nil
    var afterImage :UIImage? = nil
    
    var beforeImageView :UIImageView? = nil
    var afterImageView :UIImageView? = nil
    var shadowView :UIView? = nil
    var actionState :ActionState = .SelectPhoto
    
    var thumbnails = [NSIndexPath: UIImage]()
    var assetResults :PHFetchResult?
    let imageManager = PHImageManager()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        //enabledButtons()
        //present image picker on startup

        actionButton.layer.cornerRadius = 4.0
        addImageBarButtonItem.enabled = false
        
        setupCaptureVideoPreviewLayer()

        updateActionButton(actionState)
        
        let height = CGFloat(160.0)
        self.collectionViewHeightConstraint.constant = height
        self.collectionViewBottomConstraint.constant = -height
        self.collectionView.layoutIfNeeded()
        self.collectionView.backgroundColor = UIColor.lightGrayColor()
    }

    override func viewDidLayoutSubviews() {
        previewLayer.frame = imageView.bounds
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    @IBAction func didTapActionButton(sender: AnyObject) {
        switch actionState {
        case .SelectPhoto:
            self.selectPhoto()
            
        case .TakePhoto:
            self.takePhoto()
            
        case .FinishUp:
            self.discardPhoto()
            
        }
    }

    @IBAction func didTapAddImageBarButtonItem(sender: AnyObject) {
        saveImages()
    }

    func isOverlayImageSet() -> Bool {
        if let _ = imageView.image {
            return true
        } else {
            return false
        }
    }
    
    func layoutViews(){
        if isOverlayImageSet() == true {
            if let size = imageView.image?.size {
                let rectForImage = CGRectMake(0, 0, size.width, size.height)
                let padding = CGFloat(8.0)
                let rectForView = CGRectInset(view.bounds, padding, padding)
                let rectForResizedImage = AVMakeRectWithAspectRatioInsideRect(rectForImage.size, rectForView)
                
                imageWidthConstraint.constant = rectForResizedImage.size.width
                imageHeightConstraint.constant = rectForResizedImage.size.height
                return
            }
        }
        imageWidthConstraint.constant = 1
        imageHeightConstraint.constant = 1
    }

    func updateActionButton(_actionState :ActionState){
        actionState = _actionState
        switch actionState {
        case .SelectPhoto:
            self.actionButton.backgroundColor = UIColor.greenColor()
            self.actionButton.setTitle(" Select Photo ", forState: .Normal)
            self.addImageBarButtonItem.enabled = false
            
        case .TakePhoto:
            self.actionButton.backgroundColor = UIColor.redColor()
            self.actionButton.setTitle(" Take Photo ", forState: .Normal)
            self.addImageBarButtonItem.enabled = false
            
        case .FinishUp:
            self.actionButton.backgroundColor = UIColor.blueColor()
            self.actionButton.setTitle(" Discard ", forState: .Normal)
            self.addImageBarButtonItem.enabled = true
            
        }
    }

    //photo library methods
    func photoLibraryDidChange(changeInstance: PHChange){
        
    }
    
    func selectPhoto(){

        PHPhotoLibrary.sharedPhotoLibrary().registerChangeObserver(self)
        
        if PHPhotoLibrary.authorizationStatus() == PHAuthorizationStatus.Authorized {
            shouldDisplayImagePicker(true)
            retrieveImageThumbnails()

        } else {
            PHPhotoLibrary.requestAuthorization({
                (status: PHAuthorizationStatus) in

                dispatch_async(dispatch_get_main_queue(), {
                    switch status{
                    case .Authorized:
                        self.retrieveImageThumbnails()
                    default:
                        print("Can't access photo library")
                    }
                })
            })
            
        }
        
    }

    func retrieveNewestImage() {
        
        /* Retrieve the items in order of creation date date, newest one on top */
        let options = PHFetchOptions()
        options.sortDescriptors = [NSSortDescriptor(key: "creationDate", ascending: false)]
        
        /* Then get an object of type PHFetchResult that will contain all our image assets */
        assetResults = PHAsset.fetchAssetsWithMediaType(.Image, options: options)
        
        print("assetResults.count = \(assetResults?.count)")

        if assetResults?.count>0 {
            
            self.collectionView.reloadData()
            
            let viewSize = self.view.frame.size
            let targetSize =  viewSize
            
            let requestOptions = PHImageRequestOptions()
            requestOptions.synchronous = true
            requestOptions.version = .Current
            requestOptions.deliveryMode = .HighQualityFormat
            requestOptions.resizeMode = .Fast
            requestOptions.networkAccessAllowed = true
            requestOptions.progressHandler = {
                (progress :Double, error :NSError?, stop :UnsafeMutablePointer<ObjCBool>, info :[NSObject : AnyObject]?) in
                print("progressHandler: \(progress), \(error), \(info)")
            }
            
            if let asset = assetResults?.firstObject as? PHAsset {
                imageManager.requestImageForAsset(asset,
                    targetSize:targetSize,
                    contentMode:.AspectFill,
                    options:requestOptions,
                    resultHandler:{
                        (result :UIImage?, info :[NSObject :AnyObject]?) in
                        print("image = \(result?.size), info = \(info)\n")
                        if let image = result {
                            self.setOverlayImage(image)
                            //let indexPath :NSIndexPath = NSIndexPath(forRow: 0, inSection: 0)
                            //self.selectItemAtIndexPath(indexPath)
                        }
                })
            }
        }
        
    }

    func retrieveImageThumbnails() {
        
        /* Retrieve the items in order of creation date date, newest one on top */
        let options = PHFetchOptions()
        options.sortDescriptors = [NSSortDescriptor(key: "creationDate", ascending: false)]
        
        /* Then get an object of type PHFetchResult that will contain all our image assets */
        self.assetResults = PHAsset.fetchAssetsWithMediaType(.Image, options: options)
        
        if self.assetResults?.count>0 {
            
            let startIndex = 0
            let endIndex = 10
            
            print("enumerating thumbnails")
            self.loadImagesForIndexRange(startIndex, endIndex:endIndex)
            
        }
        
    }

    
    func loadImagesForIndexRange(startIndex :Int, endIndex :Int){
        
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_LOW, 0), {
            
            let set :NSIndexSet = NSMutableIndexSet(indexesInRange: NSRange(startIndex...endIndex))
            
            let imageManager = PHImageManager()
            
            let viewSize = self.view.frame.size
            //let targetSize =  CGSizeMake(viewSize.width/4.0, viewSize.height/4.0)
            let targetSize =  CGSizeMake(viewSize.width, viewSize.height)
            
            let requestOptions = PHImageRequestOptions()
            requestOptions.synchronous = true
            requestOptions.version = .Current
            requestOptions.deliveryMode = .Opportunistic
            requestOptions.resizeMode = .Fast
            requestOptions.networkAccessAllowed = false
            requestOptions.progressHandler = {
                (progress :Double, error :NSError?, stop :UnsafeMutablePointer<ObjCBool>, info :[NSObject : AnyObject]?) in
                print("progressHandler: \(progress), \(error), \(info)")
            }
            
            
            
            print("enumerating thumbnails")
            self.assetResults?.enumerateObjectsAtIndexes(set, options: [], usingBlock: {
                (object: AnyObject!, count: Int, stop: UnsafeMutablePointer<ObjCBool>) in
                
                if count > 25 {
                    stop.memory = true
                    
                } else {
                    if let asset = object as? PHAsset {
                        imageManager.requestImageForAsset(asset,
                            targetSize:targetSize,
                            contentMode:.AspectFill,
                            options:requestOptions,
                            resultHandler:{
                                (result :UIImage?, info :[NSObject :AnyObject]?) in
                                
                                if let image = result {
                                    
                                    print("thumbnail \(count): \(image.size)")
                                    
                                    dispatch_async(dispatch_get_main_queue(), {
                                        
                                        let indexPath = NSIndexPath(forRow:count, inSection:0)
                                        self.thumbnails[indexPath] = image
                                        
                                        if count == endIndex - startIndex - 1 {
                                            self.collectionView.reloadData()
                                        }
                                        
                                        self.shouldDisplayImagePicker(true)
                                        
                                    })
                                }
                        })
                        
                    } else {
                    }
                    
                }
                
            })
        })
        
    }
    
    func setOverlayImage(image: UIImage){
        
        self.shouldDisplayImagePicker(false)

        beforeImage = image
        print("before image selected")

        imageView.image = nil
        layoutViews()

        imageView.image = beforeImage
        imageView.alpha = 1.0
        orientation = image.imageOrientation
        processAndDisplayOverlayImage(image)
    }

    func processAndDisplayOverlayImage(image :UIImage){
        UIView.animateWithDuration(0.32, animations: {
            self.layoutViews()
            self.view.layoutIfNeeded()
            }, completion: {
                Bool in
                
                let duration = 0.24
                print("filtering before image")
                self.filteredBeforeImage = self.filterImageForOverlay(image)
                print("before image filtered")
                
                UIView.transitionWithView(self.imageView,
                    duration:duration,
                    options:.TransitionCrossDissolve,
                    animations:{
                        self.imageView.image = self.filteredBeforeImage
                    },
                    completion:{
                        Bool in
                        self.imageView.setNeedsDisplay()
                })
                
                UIView.animateWithDuration(duration, animations: {
                    self.imageView.alpha = 0.5
                    }, completion: {
                        Bool in
                        self.actionButton.enabled = true
                        
                        UIView.animateWithDuration(duration, animations: {
                            self.cameraView.alpha = 1.0
                            }, completion: {
                                Bool in
                                self.session.startRunning()
                                
                                //dispatch_after(dispatch_time_t(1.0), dispatch_get_main_queue(), {
                                    //self.shouldDisplayImagePicker(true)
                                    //self.collectionView.userInteractionEnabled = true
                                //})

                        })
                })
                
                self.updateActionButton(.TakePhoto)
                
        })
    }
    
    func autofilterImage(image :UIImage) -> UIImage {
        
        guard let cgImage = image.CGImage else { return image }
        
        var ciImage = CIImage(CGImage: cgImage, options: nil)
        
        //let options = [kCIImageAutoAdjustEnhance:true]
        let filters = ciImage.autoAdjustmentFiltersWithOptions(nil)
        for filter in filters {
            filter.setValue(ciImage, forKey:kCIInputImageKey)
            if let filteredBeforeImage = filter.outputImage {
                ciImage = filteredBeforeImage
            }
        }
        
        //ensure we pass the original image orientation to the newly created image
        let context = CIContext(options:nil)
        let tempImage:CGImageRef = context.createCGImage(ciImage, fromRect: ciImage.extent)
        let outputImage = UIImage(CGImage: tempImage, scale:image.scale, orientation:.Up)
        
        return outputImage
    }

    func filterImageForOverlay(image :UIImage) -> UIImage {
        
        guard let cgImage = image.CGImage else { return image }
        
        var ciImage = CIImage(CGImage: cgImage, options: nil)
        
        //let options = [kCIImageAutoAdjustEnhance:true]
        let filters = ciImage.autoAdjustmentFiltersWithOptions(nil)
        for filter in filters {
            filter.setValue(ciImage, forKey:kCIInputImageKey)
            if let filteredBeforeImage = filter.outputImage {
                ciImage = filteredBeforeImage
            }
        }

        if let filter = CIFilter(name:"CIUnsharpMask") {
            filter.setValue(ciImage, forKey:kCIInputImageKey)
            filter.setValue(100.0, forKey:kCIInputRadiusKey)
            filter.setValue(1.0, forKey:kCIInputIntensityKey)
            if let filteredBeforeImage = filter.outputImage {
                ciImage = filteredBeforeImage
            }
        }

        if let filter = CIFilter(name:"CISharpenLuminance") {
            filter.setValue(ciImage, forKey:kCIInputImageKey)
            filter.setValue(2.0, forKey:kCIInputSharpnessKey)
            if let filteredBeforeImage = filter.outputImage {
                ciImage = filteredBeforeImage
            }
        }
        
        if let filter = CIFilter(name:"CIColorControls"){
            filter.setValue(ciImage, forKey:kCIInputImageKey)
            filter.setValue(0.333, forKey:"inputSaturation")
            if let filteredBeforeImage = filter.outputImage {
                ciImage = filteredBeforeImage
            }
        }
        
//        if let filter = CIFilter(name:"CIGaussianBlur"){
//            filter.setValue(ciImage, forKey:kCIInputImageKey)
//            filter.setValue(3.0, forKey:"inputRadius")
//            if let filteredBeforeImage = filter.outputImage {
//                ciImage = filteredBeforeImage
//            }
//        }
        
//        if let filter = CIFilter(name:"CIColorInvert"){
//            filter.setValue(ciImage, forKey:kCIInputImageKey)
//            if let filteredBeforeImage = filter.outputImage {
//                ciImage = filteredBeforeImage
//            }
//        }

//        if let filter = CIFilter(name:"CIColorDodgeBlendMode"){
//            filter.setValue(ciImage, forKey:kCIInputImageKey)
//            filter.setValue(ciImage, forKey:kCIInputBackgroundImageKey)
//            if let filteredBeforeImage = filter.outputImage {
//                ciImage = filteredBeforeImage
//            }
//        }

//        if let filter = CIFilter(name:"CIColorDodgeBlendMode"){
//            filter.setValue(ciImage, forKey:kCIInputImageKey)
//            filter.setValue(ciImage, forKey:kCIInputBackgroundImageKey)
//            if let filteredBeforeImage = filter.outputImage {
//                ciImage = filteredBeforeImage
//            }
//        }

        
        
        /*
        case Up // default orientation
        case Down // 180 deg rotation
        case Left // 90 deg CCW
        case Right // 90 deg CW
        case UpMirrored // as above but image mirrored along other axis. horizontal flip
        case DownMirrored // horizontal flip
        case LeftMirrored // vertical flip
        case RightMirrored // vertical flip
        */
        
        //ensure we pass the original image orientation to the newly created image
        let context = CIContext(options:nil)
        let tempImage:CGImageRef = context.createCGImage(ciImage, fromRect: ciImage.extent)
        let outputImage = UIImage(CGImage: tempImage, scale:image.scale, orientation:orientation)
        
        return outputImage
    }
    
    //take photo
    func saveImage(image :UIImage, name: String){
        let documentsPath: AnyObject = NSSearchPathForDirectoriesInDomains(.DocumentDirectory,.UserDomainMask,true)[0]
        let destinationPath = documentsPath.stringByAppendingPathComponent(name)
        let destinationURL = NSURL(fileURLWithPath: destinationPath)

        if let binaryImageData = UIImagePNGRepresentation(image) {
            binaryImageData.writeToFile(destinationPath, atomically:true)
        }
        
//        if let destinationRef = CGImageDestinationCreateWithURL(destinationURL, kUTTypeJPEG, 1, nil) {
//            let properties :[String:CGFloat] = [kCGImageDestinationLossyCompressionQuality as String: 0.94]
//            CGImageDestinationSetProperties(destinationRef, properties as CFDictionaryRef)
//            CGImageDestinationAddImage(destinationRef, image.CGImage!, nil)
//            CGImageDestinationFinalize(destinationRef)
//        }

        
    }

    @IBAction func didPanView(sender: AnyObject) {
        if let _ = imageView.image {
            if let _pan = sender as? UIPanGestureRecognizer {
                let locationInView = _pan.locationInView(_pan.view)
                //print("\(_pan.view), \(_pan.state), \(_pan.locationInView(_pan.view))")
                
                let state: UIGestureRecognizerState = _pan.state
                if state == UIGestureRecognizerState.Began {
                    self.initialPanPoint = locationInView

                } else if state == UIGestureRecognizerState.Changed {
                    let alpha = locationInView.x / CGRectGetWidth(self.view.frame)
                    self.imageView.alpha = alpha
                    
                } else  {
                    self.initialPanPoint = CGPointZero
                }
            
            }
        }
    }

    func setupCaptureVideoPreviewLayer(){ 
        // For the sake of discussion this is the camera
        let device = AVCaptureDevice.defaultDeviceWithMediaType(AVMediaTypeVideo)
        
        do {
            let input: AVCaptureDeviceInput? = try AVCaptureDeviceInput(device: device)
            session.addInput(input)
        } catch _ {
            //Error handling, if needed
            fatalError("Could not create capture device input.")

        }
        
        let output = AVCaptureMetadataOutput()
        output.setMetadataObjectsDelegate(self, queue: dispatch_get_main_queue())
        session.addOutput(output)
        output.metadataObjectTypes = output.availableMetadataObjectTypes
        
        stillImageOutput.outputSettings = [AVVideoCodecKey:AVVideoCodecJPEG]
        if session.canAddOutput(stillImageOutput) {
            session.addOutput(stillImageOutput)
        }
        
        previewLayer = AVCaptureVideoPreviewLayer(session: session) as AVCaptureVideoPreviewLayer
        previewLayer.frame = self.view.bounds
        previewLayer.videoGravity = AVLayerVideoGravityResizeAspectFill
        previewLayer.frame = imageView.bounds
         cameraView.layer.insertSublayer(previewLayer, below: imageView.layer)

        previewLayer.zPosition = 1
        imageView.layer.zPosition = 2
        collectionView.layer.zPosition = 4
        actionButton.layer.zPosition = 3

//        previewLayer.borderColor = UIColor.redColor().CGColor
//        previewLayer.borderWidth = 1.0
//        imageView.layer.borderColor = UIColor.blueColor().CGColor
//        imageView.layer.borderWidth = 1.0
//        collectionView.layer.borderColor = UIColor.greenColor().CGColor
//        collectionView.layer.borderWidth = 1.0
//        actionButton.layer.borderColor = UIColor.purpleColor().CGColor
//        actionButton.layer.borderWidth = 1.0
//
//        previewLayer.cornerRadius = 8.0
//        imageView.layer.cornerRadius = 8.0
//        collectionView.layer.cornerRadius = 8.0
//        actionButton.layer.cornerRadius = 8.0

    }

    func takePhoto(){
        if let videoConnection = stillImageOutput.connectionWithMediaType(AVMediaTypeVideo) {
            stillImageOutput.captureStillImageAsynchronouslyFromConnection(videoConnection) {
                (imageDataSampleBuffer, error) -> Void in
                let imageData = AVCaptureStillImageOutput.jpegStillImageNSDataRepresentation(imageDataSampleBuffer)
                if var image = UIImage(data: imageData) {

                    print("image, size = \(image.size), imageOrientation = \(image.imageOrientation.rawValue)")

                    print("fixing after image orientation")
                    image = self.fixOrientation(image)
                    print("image, size = \(image.size), imageOrientation = \(image.imageOrientation.rawValue)")

                    print("filtering after image")
                    image = self.autofilterImage(image)
                    print("image, size = \(image.size), imageOrientation = \(image.imageOrientation.rawValue)")
                    
                    print("drawing after image")
                    let beforeImageRect = CGRectMake(0, 0, self.beforeImage!.size.width, self.beforeImage!.size.height)
                    let afterImageRect = CGRectMake(0, 0, image.size.width, image.size.height)

                    //scale aspect fit
                    var croppedImageRect = AVMakeRectWithAspectRatioInsideRect(afterImageRect.size, beforeImageRect)
                    var diff = CGFloat(0.0)
                    //now, scale aspect fill it if needed
                    if beforeImageRect.size.width>croppedImageRect.size.width {
                        diff = (beforeImageRect.size.width-croppedImageRect.size.width)/2.0
                    } else if beforeImageRect.size.height>croppedImageRect.size.height {
                        diff = (beforeImageRect.size.height-croppedImageRect.size.height)/2.0
                    }
                    croppedImageRect = CGRectIntegral(CGRectInset(croppedImageRect, -diff, -diff))
                    //print("croppedImageRect    = \(croppedImageRect)")

                    UIGraphicsBeginImageContextWithOptions(self.beforeImage!.size, false, self.beforeImage!.scale);
                    image.drawInRect(croppedImageRect)


                    if let croppedCGImage = UIGraphicsGetImageFromCurrentImageContext().CGImage {

                        //self.afterImage = self.fixOrientation(croppedImage)
                        self.afterImage = UIImage(CGImage:croppedCGImage, scale:0, orientation:.Up)

                        print("after image ready")

                        print("self.afterImage?, size = \(self.afterImage?.size), imageOrientation = \(self.afterImage?.imageOrientation.rawValue)")

                        self.presentBeforeAfterImages()
                    }
                    UIGraphicsEndImageContext()
                    

                } else {
                    fatalError("unable to save after image")
                }

            }
        }
    }

    func presentBeforeAfterImages(){

        session.stopRunning()
        
        let _beforeImageView = UIImageView(image: beforeImage)
        let _afterImageView = UIImageView(image: afterImage)
        let _shadowView = UIView(frame: CGRectZero)
        _shadowView.backgroundColor = UIColor.darkGrayColor()

        beforeImageView = _beforeImageView
        afterImageView = _afterImageView
        shadowView = _shadowView

        _beforeImageView.contentMode = .ScaleToFill
        _afterImageView.contentMode = .ScaleToFill

        self.view.addSubview(_beforeImageView)
        self.view.addSubview(_afterImageView)
        self.view.addSubview(_shadowView)

        let frame = imageView.frame
        _beforeImageView.frame = frame
        _afterImageView.frame = frame
        
        var finalBeforeFrame = _beforeImageView.frame
        finalBeforeFrame.size.width /= 2.0
        finalBeforeFrame.size.height /= 2.0
        finalBeforeFrame.origin.x = frame.origin.x
        finalBeforeFrame.origin.y = frame.origin.y + (frame.size.height - finalBeforeFrame.size.height)/2.0
        
        var finalAfterFrame = _afterImageView.frame
        finalAfterFrame.size.width /= 2.0
        finalAfterFrame.size.height /= 2.0
        finalAfterFrame.origin.x = frame.origin.x + finalAfterFrame.size.width
        finalAfterFrame.origin.y = frame.origin.y + (frame.size.height - finalBeforeFrame.size.height)/2.0
        
        _shadowView.frame = CGRectUnion(finalBeforeFrame, finalAfterFrame)
        _shadowView.layer.shadowColor = UIColor.blackColor().CGColor
        _shadowView.layer.shadowOpacity = 0.5
        _shadowView.layer.shadowOffset = CGSizeMake(0,2)
        _shadowView.layer.shadowRadius = 3.0
        _shadowView.alpha = 0.0
        _shadowView.transform = CGAffineTransformTranslate(CGAffineTransformMakeScale(0.9, 0.9),0.0,32.0)

        _shadowView.layer.zPosition = 20
        _beforeImageView.layer.zPosition = 21
        _afterImageView.layer.zPosition = 22

        UIView.animateWithDuration(0.96, animations: {
            _beforeImageView.frame = finalBeforeFrame
            _afterImageView.frame = finalAfterFrame
            _shadowView.alpha = 1.0
            _shadowView.transform = CGAffineTransformIdentity
            }, completion: {
                Bool in
                self.updateActionButton(.FinishUp)
        })
        UIView.animateWithDuration(0.16, animations: {
            self.imageView.alpha = 0.0
            self.cameraView.alpha = 0.0
            }, completion: {
                Bool in
                print("self.beforeImage.size = \(self.beforeImage!.size)")
                print("self.afterImage.size  = \(self.afterImage!.size)")
        })
        
    }
    
    func saveImages(){
        self.addImageBarButtonItem.enabled = false
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_LOW, 0), {
            
            if let image = self.beforeImage {
                print("image0-original.png size = \(image.size)")
                self.saveImage(image, name: "image0-original.png")
            }
            
            if let image = self.afterImage {
                print("image1-original.png size = \(image.size)")
                UIImageWriteToSavedPhotosAlbum(image, nil, nil, nil)
                //self.saveImage(image, name: "image1-original.png")
            }
        })
    }

    func discardPhoto() {

        print("self.beforeImage.size = \(self.beforeImage!.size)")
        print("self.afterImage.size  = \(self.afterImage!.size)")

        let xTranslate = CGFloat(self.view.bounds.width/2.0)
        let yTranslate = CGFloat(0.0)
        let beforeImageTransform = CGAffineTransformScale(CGAffineTransformTranslate(CGAffineTransformMakeRotation(-0.2), -xTranslate, yTranslate), 0.75, 0.75)
        let afterImageTransform = CGAffineTransformScale(CGAffineTransformTranslate(CGAffineTransformMakeRotation(0.2), xTranslate, yTranslate), 0.75, 0.75)
        let shadowImageTransform = CGAffineTransformTranslate(CGAffineTransformMakeScale(0.75, 0.75), 0.0, 32)

        UIView.animateWithDuration(0.12, animations: {
            self.shadowView!.alpha = 0.0
            self.shadowView!.transform = shadowImageTransform
            })

        UIView.animateWithDuration(0.32, animations: {
            self.beforeImageView!.alpha = 0.0
            self.afterImageView!.alpha = 0.0
            self.beforeImageView!.transform = beforeImageTransform
            self.afterImageView!.transform = afterImageTransform
            }, completion: {
                Bool in
                self.beforeImageView!.removeFromSuperview()
                self.afterImageView!.removeFromSuperview()
                self.shadowView?.removeFromSuperview()
                self.beforeImageView = nil
                self.afterImageView = nil
                self.shadowView = nil

                self.beforeImage = nil
                self.filteredBeforeImage = nil
                self.afterImage = nil
                self.imageView.image = nil
                self.updateActionButton(.SelectPhoto)
                self.addImageBarButtonItem.enabled = false

        })
        
    }

    func fixOrientation(image: UIImage) -> UIImage{
        
        let imageOrientation = image.imageOrientation
        
//        if image.imageOrientation == UIImageOrientation.Up {
//            return image
//        }
        
        var transform = CGAffineTransformIdentity
        
        switch imageOrientation {
        case .Down, .DownMirrored:
            transform = CGAffineTransformTranslate(transform, image.size.width, image.size.height)
            transform = CGAffineTransformRotate(transform, CGFloat(M_PI));
            
        case .Left, .LeftMirrored:
            transform = CGAffineTransformTranslate(transform, image.size.width, 0);
            transform = CGAffineTransformRotate(transform, CGFloat(M_PI_2));
            
        case .Right, .RightMirrored:
            transform = CGAffineTransformTranslate(transform, image.size.width/2.0,image.size.height/2.0);
            if image.size.width > image.size.height {
                transform = CGAffineTransformRotate(transform, CGFloat(M_PI/2.0))
            } else {
                transform = CGAffineTransformRotate(transform, CGFloat(-M_PI/2.0))
            }
            transform = CGAffineTransformTranslate(transform, -image.size.height/2.0,-image.size.width/2.0);

        case .Up, .UpMirrored:
            //transform = CGAffineTransformTranslate(transform, image.size.width/2.0, image.size.height/2.0)
            //transform = CGAffineTransformTranslate(transform, -image.size.width/2.0, -image.size.height/2.0)
            //transform = CGAffineTransformTranslate(transform, -image.size.width/2.0, -image.size.height/2.0)
            //transform = CGAffineTransformTranslate(transform, image.size.width, image.size.height)
            //transform = CGAffineTransformRotate(transform, CGFloat(-M_PI/2.0));
//            transform = CGAffineTransformRotate(transform, CGFloat(M_PI/2.0));
//            transform = CGAffineTransformTranslate(transform, 0, -image.size.height)
//            transform = CGAffineTransformTranslate(transform, 0, image.size.height)
//            transform = CGAffineTransformTranslate(transform, image.size.width, 0)
//            transform = CGAffineTransformTranslate(transform, -image.size.width, 0)
//            transform = CGAffineTransformTranslate(transform, image.size.height, 0)
//            transform = CGAffineTransformTranslate(transform, -image.size.height, 0)
//            transform = CGAffineTransformTranslate(transform, 0, -image.size.width)
//            transform = CGAffineTransformTranslate(transform, 0, image.size.width)

            
            transform = CGAffineTransformTranslate(transform, image.size.width/2.0,image.size.height/2.0);
            transform = CGAffineTransformRotate(transform, CGFloat(-M_PI/2.0));
            transform = CGAffineTransformTranslate(transform, -image.size.height/2.0,-image.size.width/2.0);
            
            break
        }
        
        
        switch imageOrientation {
            
        case .UpMirrored, .DownMirrored:
            transform = CGAffineTransformTranslate(transform, image.size.width, 0)
            transform = CGAffineTransformScale(transform, -1, 1)
            
        case .LeftMirrored, .RightMirrored:
            transform = CGAffineTransformTranslate(transform, image.size.height, 0)
            transform = CGAffineTransformScale(transform, -1, 1);
            
        default:
            break;
        }
        
        
        
        // Now we draw the underlying CGImage into a new context, applying the transform
        // calculated above.
        let ctx = CGBitmapContextCreate(
            nil,
            Int(image.size.width),
            Int(image.size.height),
            CGImageGetBitsPerComponent(image.CGImage),
            0,
            CGImageGetColorSpace(image.CGImage),
            UInt32(CGImageGetBitmapInfo(image.CGImage).rawValue)
        )
        
        CGContextConcatCTM(ctx, transform);
        
        switch imageOrientation {
            
        case .Left, .LeftMirrored, .Right, .RightMirrored:
            // Grr...
            CGContextDrawImage(ctx, CGRectMake(0, 0, image.size.height,image.size.width), image.CGImage);
            
        default:
            CGContextDrawImage(ctx, CGRectMake(0, 0, image.size.width,image.size.height), image.CGImage);
            break;
        }
        
        // And now we just create a new UIImage from the drawing context
        let cgimg = CGBitmapContextCreateImage(ctx)
        
        let img = UIImage(CGImage: cgimg!)
        
//        CGContextRelease(ctx)
//        CGImageRelease(cgimg)
        
        return img;
        
    }

    //collection view data source
    func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if assetResults != nil {
            return assetResults!.count
        } else {
            return 0
        }
    }
    
    func collectionView(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCellWithReuseIdentifier("thumbnailCellIdentifier", forIndexPath: indexPath) as! ThumbnailCell
        if let image = thumbnails[indexPath] {
            cell.thumbnailImageView.image = image
            
            cell.layer.borderColor = UIColor.whiteColor().CGColor
            cell.layer.borderWidth = 2.0
            
            cell.indicatorImageView.layer.borderColor = UIColor.whiteColor().CGColor
            cell.indicatorImageView.layer.borderWidth = 2.0
            cell.indicatorImageView.layer.cornerRadius = CGRectGetWidth(cell.indicatorImageView.frame)/2.0
        } else {
            cell.thumbnailImageView.image = nil
        }
        
        return cell
    }
    
    func numberOfSectionsInCollectionView(collectionView: UICollectionView) -> Int {
        return 1
    }

    //collection view delegate
    func collectionView(collectionView: UICollectionView, didSelectItemAtIndexPath indexPath: NSIndexPath) {
        if let image = thumbnails[indexPath] {
            setOverlayImage(image)
            selectItemAtIndexPath(indexPath)
        }
    }

    func collectionView(collectionView: UICollectionView, didDeselectItemAtIndexPath indexPath: NSIndexPath) {
        if let cell = collectionView.cellForItemAtIndexPath(indexPath) as? ThumbnailCell {
            cell.shouldSetSelected(false)
            deselectItemAtIndexPath(indexPath)
        }
    }

    func selectItemAtIndexPath(indexPath: NSIndexPath) {
        if let cell = collectionView.cellForItemAtIndexPath(indexPath) as? ThumbnailCell{
            cell.shouldSetSelected(true)
        }
        if let indexPathsForSelectedItems = collectionView.indexPathsForSelectedItems() {
            for selectedIndexPath in indexPathsForSelectedItems {
                if selectedIndexPath != indexPath {
                    deselectItemAtIndexPath(selectedIndexPath)
                }
            }
        }
    }
    
    func deselectItemAtIndexPath(indexPath: NSIndexPath) {
        if let cell = collectionView.cellForItemAtIndexPath(indexPath) as? ThumbnailCell {
            cell.shouldSetSelected(false)
        }
    }

    //collection view delegate flow layout
    func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAtIndexPath indexPath: NSIndexPath) -> CGSize {
        if let image = thumbnails[indexPath] {
            let scale = CGRectGetHeight(collectionView.frame) - 10.0
            let size = CGSizeMake(image.size.width/image.size.height * scale, scale)
            return size
        } else {
            return CGSizeMake(80, 100)
        }
    }

    func shouldDisplayImagePicker(display :Bool){
        if display {
            self.collectionViewBottomConstraint.constant = 0.0
            UIView.animateWithDuration(0.24,
                delay: 0.0,
                usingSpringWithDamping: 0.5,
                initialSpringVelocity: 0.5,
                options: .BeginFromCurrentState,
                animations: {
                    self.collectionView.layoutIfNeeded()
                }, completion: {
                    (finished :Bool) in
                    
            })

        } else {

            self.collectionViewBottomConstraint.constant = -CGRectGetHeight(self.collectionView.frame)
            UIView.animateWithDuration(0.64,
                delay: 0.0,
                usingSpringWithDamping: 0.5,
                initialSpringVelocity: 0.5,
                options: .BeginFromCurrentState,
                animations: {
                    self.collectionView.layoutIfNeeded()
                }, completion: {
                    (finished :Bool) in
            })

        }
    }
    
}
