//
//  BeforeAfterStyleKit.swift
//  BeforeAfter
//
//  Created by Liam Dunne on 09/03/2016.
//  Copyright (c) 2016 Lmd64. All rights reserved.
//
//  Generated by PaintCode (www.paintcodeapp.com)
//



import UIKit

public class BeforeAfterStyleKit : NSObject {

    //// Cache

    private struct Cache {
        static let defaultColor: UIColor = UIColor(red: 0.405, green: 0.490, blue: 0.546, alpha: 1.000)
        static let lightDefaultColor: UIColor = UIColor(red: 0.664, green: 0.709, blue: 0.738, alpha: 1.000)
        static let lightDefaultColorWithAlpha: UIColor = UIColor(red: 0.663, green: 0.710, blue: 0.737, alpha: 0.251)
        static let shadow: NSShadow = NSShadow(color: UIColor.blackColor().colorWithAlphaComponent(0.5), offset: CGSizeMake(0.1, 2.1), blurRadius: 1)
    }

    //// Colors

    public class var defaultColor: UIColor { return Cache.defaultColor }
    public class var lightDefaultColor: UIColor { return Cache.lightDefaultColor }
    public class var lightDefaultColorWithAlpha: UIColor { return Cache.lightDefaultColorWithAlpha }

    //// Shadows

    public class var shadow: NSShadow { return Cache.shadow }

    //// Drawing Methods

    public class func drawCameraButton(frame frame: CGRect = CGRectMake(0, 0, 64, 64)) {

        //// Oval Drawing
        let ovalPath = UIBezierPath(ovalInRect: CGRectMake(frame.minX + floor((frame.width - 54) * 0.50000 + 0.5), frame.minY + floor((frame.height - 56) * 0.50000 + 0.5), 54, 56))
        UIColor.whiteColor().setFill()
        ovalPath.fill()
        BeforeAfterStyleKit.defaultColor.setStroke()
        ovalPath.lineWidth = 6
        ovalPath.stroke()


        //// Oval 2 Drawing
        let oval2Path = UIBezierPath(ovalInRect: CGRectMake(frame.minX + floor((frame.width - 42) * 0.50000 + 0.5), frame.minY + floor((frame.height - 44) * 0.50000 + 0.5), 42, 44))
        BeforeAfterStyleKit.defaultColor.setFill()
        oval2Path.fill()
    }

    public class func drawAddButton(frame frame: CGRect = CGRectMake(0, 0, 64, 64)) {
        //// General Declarations
        let context = UIGraphicsGetCurrentContext()

        //// Oval Drawing
        let ovalPath = UIBezierPath(ovalInRect: CGRectMake(frame.minX + floor((frame.width - 62) * 0.50000 + 0.5), frame.minY + floor((frame.height - 62) * 0.50000 + 0.5), 62, 62))
        UIColor.whiteColor().setFill()
        ovalPath.fill()


        //// Rectangle Drawing
        CGContextSaveGState(context)
        CGContextTranslateCTM(context, frame.minX + 0.50000 * frame.width, frame.minY + 0.50000 * frame.height)

        let rectanglePath = UIBezierPath(roundedRect: CGRectMake(-4, -28, 8, 56), cornerRadius: 4)
        BeforeAfterStyleKit.defaultColor.setFill()
        rectanglePath.fill()

        CGContextRestoreGState(context)


        //// Rectangle 2 Drawing
        CGContextSaveGState(context)
        CGContextTranslateCTM(context, frame.minX + 0.50000 * frame.width, frame.minY + 0.50000 * frame.height)

        let rectangle2Path = UIBezierPath(roundedRect: CGRectMake(-27, -4, 54, 8), cornerRadius: 4)
        BeforeAfterStyleKit.defaultColor.setFill()
        rectangle2Path.fill()

        CGContextRestoreGState(context)
    }

    public class func drawDiscardButton(frame frame: CGRect = CGRectMake(0, 0, 64, 64)) {
        //// General Declarations
        let context = UIGraphicsGetCurrentContext()

        //// Oval Drawing
        let ovalPath = UIBezierPath(ovalInRect: CGRectMake(frame.minX + floor((frame.width - 62) * 0.50000 + 0.5), frame.minY + floor((frame.height - 62) * 0.50000 + 0.5), 62, 62))
        UIColor.whiteColor().setFill()
        ovalPath.fill()


        //// Group
        CGContextSaveGState(context)
        CGContextTranslateCTM(context, frame.minX + 0.50000 * frame.width, frame.minY + 0.50000 * frame.height)
        CGContextRotateCTM(context, -45 * CGFloat(M_PI) / 180)



        //// Rectangle Drawing
        CGContextSaveGState(context)

        let rectanglePath = UIBezierPath(roundedRect: CGRectMake(-4, -28, 8, 56), cornerRadius: 4)
        BeforeAfterStyleKit.defaultColor.setFill()
        rectanglePath.fill()

        CGContextRestoreGState(context)


        //// Rectangle 2 Drawing
        CGContextSaveGState(context)

        let rectangle2Path = UIBezierPath(roundedRect: CGRectMake(-27, -4, 54, 8), cornerRadius: 4)
        BeforeAfterStyleKit.defaultColor.setFill()
        rectangle2Path.fill()

        CGContextRestoreGState(context)



        CGContextRestoreGState(context)
    }

    public class func drawTickIndicatorDeselected(frame frame: CGRect = CGRectMake(0, 0, 20, 20)) {

        //// Oval 2 Drawing
        let oval2Path = UIBezierPath(ovalInRect: CGRectMake(frame.minX + floor((frame.width - 18) * 0.50000 + 0.5), frame.minY + floor((frame.height - 18) * 0.50000 + 0.5), 18, 18))
        BeforeAfterStyleKit.lightDefaultColor.setFill()
        oval2Path.fill()
    }

    public class func drawTickIndicatorSelected(frame frame: CGRect = CGRectMake(0, 0, 20, 20)) {
        //// General Declarations
        let context = UIGraphicsGetCurrentContext()


        //// Subframes
        let group: CGRect = CGRectMake(frame.minX + floor((frame.width - 10.78) * 0.57579 + 0.19) + 0.31, frame.minY + floor((frame.height - 8.49) * 0.51431 - 0.09) + 0.59, 10.78, 8.49)


        //// Oval 2 Drawing
        let oval2Path = UIBezierPath(ovalInRect: CGRectMake(frame.minX + floor((frame.width - 18) * 0.50000 + 0.5), frame.minY + floor((frame.height - 18) * 0.50000 + 0.5), 18, 18))
        BeforeAfterStyleKit.defaultColor.setFill()
        oval2Path.fill()


        //// Group
        //// Rectangle Drawing
        CGContextSaveGState(context)
        CGContextTranslateCTM(context, group.minX + 3.71, group.minY + 7.07)
        CGContextRotateCTM(context, 45 * CGFloat(M_PI) / 180)

        let rectanglePath = UIBezierPath(roundedRect: CGRectMake(-1, -9, 2, 10), cornerRadius: 1)
        UIColor.whiteColor().setFill()
        rectanglePath.fill()

        CGContextRestoreGState(context)


        //// Rectangle 2 Drawing
        CGContextSaveGState(context)
        CGContextTranslateCTM(context, group.minX + 4.05, group.minY + 7.41)
        CGContextRotateCTM(context, -45 * CGFloat(M_PI) / 180)

        let rectangle2Path = UIBezierPath(roundedRect: CGRectMake(-1, -4.73, 2, 5.25), cornerRadius: 1)
        UIColor.whiteColor().setFill()
        rectangle2Path.fill()

        CGContextRestoreGState(context)
    }

    public class func drawSlider(value value: CGFloat = 0) {
        //// General Declarations
        let context = UIGraphicsGetCurrentContext()


        //// Variable Declarations
        let scaledValue: CGFloat = value * (320 - 16 * 2) + 16
        let scaledWidth: CGFloat = max(24, value * (320 - 32) + 24)

        //// Rectangle Drawing
        CGContextSaveGState(context)
        CGContextTranslateCTM(context, 160, 16)

        let rectanglePath = UIBezierPath(roundedRect: CGRectMake(-160, -16, 320, 32), cornerRadius: 16)
        UIColor.whiteColor().setFill()
        rectanglePath.fill()

        CGContextRestoreGState(context)


        //// Rectangle 3 Drawing
        CGContextSaveGState(context)
        CGContextTranslateCTM(context, 160, 16)
        CGContextRotateCTM(context, -0.01 * CGFloat(M_PI) / 180)

        let rectangle3Path = UIBezierPath(roundedRect: CGRectMake(-156, -12, 312, 24), cornerRadius: 12)
        BeforeAfterStyleKit.lightDefaultColorWithAlpha.setFill()
        rectangle3Path.fill()

        CGContextRestoreGState(context)


        //// Rectangle 2 Drawing
        CGContextSaveGState(context)
        CGContextTranslateCTM(context, 16, 16)

        let rectangle2Path = UIBezierPath(roundedRect: CGRectMake(-12, -12, scaledWidth, 24), cornerRadius: 12)
        BeforeAfterStyleKit.lightDefaultColor.setFill()
        rectangle2Path.fill()

        CGContextRestoreGState(context)


        //// Oval Drawing
        CGContextSaveGState(context)
        CGContextTranslateCTM(context, scaledValue, 16)

        let ovalPath = UIBezierPath(ovalInRect: CGRectMake(-12, -12, 24, 24))
        BeforeAfterStyleKit.defaultColor.setFill()
        ovalPath.fill()

        CGContextRestoreGState(context)
    }

    public class func drawAlert(frame frame: CGRect = CGRectMake(0, 0, 240, 140)) {
        //// General Declarations
        let context = UIGraphicsGetCurrentContext()

        //// Rectangle Drawing
        let rectanglePath = UIBezierPath(roundedRect: CGRectMake(frame.minX, frame.minY, frame.width, frame.height), cornerRadius: 8)
        BeforeAfterStyleKit.lightDefaultColor.setFill()
        rectanglePath.fill()


        //// Rectangle 2 Drawing
        let rectangle2Path = UIBezierPath(roundedRect: CGRectMake(frame.minX + 8, frame.minY + 8, frame.width - 16, frame.height - 56), cornerRadius: 8)
        UIColor.whiteColor().setFill()
        rectangle2Path.fill()


        //// Text Drawing
        let textRect = CGRectMake(frame.minX + 12, frame.minY + 12, frame.width - 24, 32)
        let textTextContent = NSString(string: "Alert Title")
        let textStyle = NSParagraphStyle.defaultParagraphStyle().mutableCopy() as! NSMutableParagraphStyle
        textStyle.alignment = .Center

        let textFontAttributes = [NSFontAttributeName: UIFont.boldSystemFontOfSize(UIFont.systemFontSize()), NSForegroundColorAttributeName: BeforeAfterStyleKit.defaultColor, NSParagraphStyleAttributeName: textStyle]

        let textTextHeight: CGFloat = textTextContent.boundingRectWithSize(CGSizeMake(textRect.width, CGFloat.infinity), options: NSStringDrawingOptions.UsesLineFragmentOrigin, attributes: textFontAttributes, context: nil).size.height
        CGContextSaveGState(context)
        CGContextClipToRect(context, textRect);
        textTextContent.drawInRect(CGRectMake(textRect.minX, textRect.minY + (textRect.height - textTextHeight) / 2, textRect.width, textTextHeight), withAttributes: textFontAttributes)
        CGContextRestoreGState(context)


        //// Text 2 Drawing
        let text2Rect = CGRectMake(frame.minX + 12, frame.minY + 42, frame.width - 24, frame.height - 96)
        let text2Style = NSParagraphStyle.defaultParagraphStyle().mutableCopy() as! NSMutableParagraphStyle
        text2Style.alignment = .Center

        let text2FontAttributes = [NSFontAttributeName: UIFont.systemFontOfSize(UIFont.smallSystemFontSize()), NSForegroundColorAttributeName: BeforeAfterStyleKit.defaultColor, NSParagraphStyleAttributeName: text2Style]

        "Alert Message".drawInRect(text2Rect, withAttributes: text2FontAttributes)


        //// Rectangle 3 Drawing
        CGContextSaveGState(context)
        CGContextTranslateCTM(context, frame.minX + 0.50000 * frame.width, frame.maxY - 24)

        let rectangle3Path = UIBezierPath(roundedRect: CGRectMake(-33, -16, 66, 32), cornerRadius: 8)
        BeforeAfterStyleKit.defaultColor.setFill()
        rectangle3Path.fill()

        CGContextRestoreGState(context)


        //// Text 3 Drawing
        let text3Rect = CGRectMake(frame.minX + floor((frame.width - 66) * 0.50000 + 0.5), frame.minY + frame.height - 40, 66, 32)
        let text3TextContent = NSString(string: "Ok")
        let text3Style = NSParagraphStyle.defaultParagraphStyle().mutableCopy() as! NSMutableParagraphStyle
        text3Style.alignment = .Center

        let text3FontAttributes = [NSFontAttributeName: UIFont.boldSystemFontOfSize(UIFont.smallSystemFontSize()), NSForegroundColorAttributeName: UIColor.whiteColor(), NSParagraphStyleAttributeName: text3Style]

        let text3TextHeight: CGFloat = text3TextContent.boundingRectWithSize(CGSizeMake(text3Rect.width, CGFloat.infinity), options: NSStringDrawingOptions.UsesLineFragmentOrigin, attributes: text3FontAttributes, context: nil).size.height
        CGContextSaveGState(context)
        CGContextClipToRect(context, text3Rect);
        text3TextContent.drawInRect(CGRectMake(text3Rect.minX, text3Rect.minY + (text3Rect.height - text3TextHeight) / 2, text3Rect.width, text3TextHeight), withAttributes: text3FontAttributes)
        CGContextRestoreGState(context)
    }

}



extension NSShadow {
    convenience init(color: AnyObject!, offset: CGSize, blurRadius: CGFloat) {
        self.init()
        self.shadowColor = color
        self.shadowOffset = offset
        self.shadowBlurRadius = blurRadius
    }
}
